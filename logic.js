//TODO general: more animations would be nice

const states = {
    WAITING_FOR_INPUT: 'input',
    DISPLAYING_RESULTS: 'results',
    DISPLAYING_WINNER: 'winner', //TODO would be nice to have some seconds to comprehend which map won
    WAITING_FOR_NEXT_VOTE: 'waiting'
};

let state = states.WAITING_FOR_NEXT_VOTE;
let secondsRemaining;

$(document).ready(function () {
    checkForVote();
});

function timer() {
    if(secondsRemaining > 0) {
        if(secondsRemaining === 1) {
            $("#progress").html("<h2>" + secondsRemaining +" second left</h2>"); //Someone would notice that and complain
        }else {
            $("#progress").html("<h2>" + secondsRemaining +" seconds left</h2>");
        }
        secondsRemaining = secondsRemaining - 1;
        setTimeout(() => {
            timer()
        }, 1000);
    } else if(state !== states.WAITING_FOR_NEXT_VOTE) {
        state = states.WAITING_FOR_NEXT_VOTE;
        checkForVote();
    }
}

/**
 * Update loop
 */
function checkForVote() {
    //Only check if displaying results or waiting for next vote
    if (state === states.DISPLAYING_RESULTS || state === states.WAITING_FOR_NEXT_VOTE) {
        $.get("http://localhost:8080/vote").done(function (data) {
            console.log(secondsRemaining);
            console.log(data);
            if(data){
                if(!secondsRemaining || secondsRemaining < 0){
                    const now = new Date();
                    now.setTime( now.getTime() + now.getTimezoneOffset()*60*1000 ); //We get UTC from the backend so convert to that
                    const dif = now.getTime() - new Date(data.voteStart).getTime();
                    secondsRemaining = data.voteDurationSeconds - Math.ceil(dif / 1000); //Better end a few ms early than too late
                    timer();
                }

                if (state === states.DISPLAYING_RESULTS) {
                    $("#maps-grid").html(data.maps.map(map => {
                        return '<div class="map-wrapper" id="' + map.id + '"' +
                            'style="background-image: linear-gradient(black, black), url(https://assets.ppy.sh/beatmaps/' + map.setId + '/covers/cover.jpg); background-blend-mode: saturation; order: ' + map.voteOrder + ';">' +
                            '<div class="result" style="background: linear-gradient(90deg, rgba(0,0,0,0.6) 0%, rgba(1,255,0,0.4) 0%, rgba(100,255,100,0.4) ' + map.votePercent + '%, rgba(0,0,0,0.6) ' + map.votePercent + '%, rgba(0,0,0,0.6) 100%);">' +
                            '<div class="title">' +
                            '<div class="map-type">' +
                            map.mapType +
                            '</div>' +
                            '<h2>' + map.name + '</h2>' +
                            '<p>' + map.artist + '</p>' +
                            '<p>' + map.difficulty + '</p>' +
                            '</div>' +
                            '<div class="percentage"><h1>'+ map.votePercent +'%</h1></div>' +
                            '</div>' +
                            '</div>';
                    }).join(''));
                    $("#description").html("<h1>Results</h1>")
                } else {
                    if (data.voteRunning && secondsRemaining > 0) {
                        $("#maps-grid").html(data.maps.map(map => {
                            return '<div class="map-wrapper" id="' + map.id + '"' +
                                'style="background-image: url(https://assets.ppy.sh/beatmaps/' + map.setId + '/covers/cover.jpg)">' +
                                '<div class="map">' +
                                '<div class="title">' +
                                '<div class="map-type">' +
                                map.mapType +
                                '</div>' +
                                '<h2>' + map.name + '</h2>' +
                                '<p>' + map.artist + '</p>' +
                                '<p>' + map.difficulty + '</p>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                        }).join(''));
                        $("#description").html("<h1>Vote for the next map</h1>");
                        $(".app-wrapper").css({"display": "flex"});
                        setEventHandlerFor(data.maps);
                        state = states.WAITING_FOR_INPUT;
                    } else {
                        $(".app-wrapper").css({"display": "none"});
                    }
                }
            }
        });
        setTimeout(() => {
            checkForVote()
        }, 2000);
    }
}

function setEventHandlerFor(maps) {
    maps.forEach(map => {
        $("#" + map.id).click(function () {
            //TODO check the JWT the opaqueId is taken from at the EBS so it can't be faked
            if (Twitch.ext.viewer.opaqueId == null) {
                alert("You need to be logged in to vote");
            } else {
                $.post("http://localhost:8080/vote?mapId=" + map.id + '&userId=' + Twitch.ext.viewer.opaqueId).done(function () {
                    state = states.DISPLAYING_RESULTS;
                    checkForVote();
                });
            }
        });
    });
}

