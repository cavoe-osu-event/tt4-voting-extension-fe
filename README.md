# Quick things
The program currently points to a backend running at localhost:8080. This needs to be replaced with an actual backend later on.
An example backend is provided [here](https://gitlab.com/cavoe-osu-event/tt4-voting-extension-be-example)
It currently needs a backend to support an GET /vote endpoint and an POST /vote endpoint.

GET /vote just retrieves data about the current vote running (see example JSON below)
POST /vote will send a vote to the server and needs the mapId and userId urlEncoded

```json
{
    "voteRunning": true,
    "maps": [
        {
            "id": 2220287,
            "setId": 1060431,
            "name": "Pastel Rain",
            "artist": "Sangatsu no Phantasia",
            "difficulty": "[Extra]",
            "mapType": "FM",
            "votePercent": 0,
            "voteOrder": 0,
            "votes": 0
        },
        {
            "id": 2658939,
            "setId": 1280001,
            "name": "cold weather",
            "artist": "glass beach",
            "difficulty": "[extra]",
            "mapType": "DT",
            "votePercent": 0,
            "voteOrder": 0,
            "votes": 0
        },
        {
            "id": 2608105,
            "setId": 1254930,
            "name": "You (=I)",
            "artist": "BOL4",
            "difficulty": "[Insane]",
            "mapType": "RX",
            "votePercent": 0,
            "voteOrder": 0,
            "votes": 0
        },
        {
            "id": 2650207,
            "setId": 1275523,
            "name": "smooooch -Akiba Koubou mix- / Remixed by DJ Command",
            "artist": "kors k",
            "difficulty": "[move your body]",
            "mapType": "HD",
            "votePercent": 0,
            "voteOrder": 0,
            "votes": 0
        }
    ],
    "voteDurationSeconds": 30,
    "voteStart": "2020-11-20T09:14:56.972273"
}
```
